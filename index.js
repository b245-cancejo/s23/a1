//console.log("hello")

let trainer = {
	name: "Ash Ketchum",
	age: 12,
	pokemon:['Pikachu', 'Charizard', 'Squirtle', 'Bulbasaur'],
	friends: {
		hoenn:["May", "Max"],
		kanto:["Brock", "Misty"]
	},
	talk: function() {
		console.log("Pikachu! I choose you!");
	}
}
console.log(trainer);
console.log("Result of dot notation:")
console.log(trainer.name);

console.log("Result of square bracket notation:");
console.log(trainer["pokemon"]);


console.log("Result of talk method");
trainer.talk();


function Pokemon(name, level){
	this.pokemonName = name;
	this.pokemonLevel = level;
	this.pokemonHealth = 2 * level;
	this.pokemonAttack = level;

	this.tackle = function(targetPokemon){
				console.log(this.pokemonName + " tackles " + targetPokemon.pokemonName);

				let hpAftertackle = targetPokemon.pokemonHealth- this.pokemonAttack;

				console.log(targetPokemon.pokemonName + "'s health is now reduced to " + hpAftertackle);

				if(hpAftertackle <= 0){
        			targetPokemon.faint();
        			targetPokemon.pokemonHealth = hpAftertackle;
        			console.log(targetPokemon);
    				}
    			}

	this.faint = function(){
		console.log(this.pokemonName + " fainted!" )
		}			
	
}
			

let pikachu = new Pokemon("Pikachu", 12);
console.log(pikachu);

let geodude = new Pokemon("Geodude", 8);
console.log(geodude);

let mewtwo = new Pokemon("Mewtwo", 100);
console.log(mewtwo);

geodude.tackle(pikachu);
console.log(pikachu);

mewtwo.tackle(geodude);




